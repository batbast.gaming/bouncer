package eu.tyrano.bouncer;

public class SuperMain {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Launching client...");
            eu.tyrano.bouncer.client.MainClient.main(args);
        } else {
            if (args[0].equals("--server")) {
                System.out.println("Launching server...");
                eu.tyrano.bouncer.server.Bouncer.main(args);
            } else {
                System.out.println("Launching client...");
                eu.tyrano.bouncer.client.MainClient.main(args);
            }
        }
    }
}
