package eu.tyrano.bouncer.client;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.util.ArrayList;

public class Ball {

    private double x;
    private double y;
    private final double size;
    private final ArrayList<BallPath> ballPaths;
    private final Color color;

    public Ball(double size, Color color) {
        this.x = 0;
        this.y = 0;
        this.size = size;
        this.color = color;
        this.ballPaths = new ArrayList<>();
    }

    public void newBallPath(double x, double y, double size) {
        this.ballPaths.add(new BallPath(x, y, size, this.color));
    }

    public void move(double x, double y) {
        this.x = x;
        this.y = y;

        this.ballPaths.add(new BallPath(x, y, size / 10, color));
    }

    public Node[] display(int width, int height) {
        Node[] nodes = new Node[1 + this.ballPaths.size()];

        double xToPrint = ((width - height) / 2d) + (height / 1000d * (this.x + 500));
        double yToPrint = height - (height / 1000d * (this.y + 500));
        double sizeToPrint = (height / 1000d * this.size);

        Circle circle = new Circle(xToPrint, yToPrint, sizeToPrint);

        circle.setFill(this.color);

        nodes[0] = circle;

        ArrayList<BallPath> toRemove = new ArrayList<>();


        try {
            for (int i = 1; i < this.ballPaths.size() + 1; i++) {
                if (this.ballPaths.get(i - 1).isDead()) {
                    toRemove.add(this.ballPaths.get(i - 1));
                }
                nodes[i] = this.ballPaths.get(i - 1).display(width, height);
            }
        } catch (Exception ignored) {

        }

        for (BallPath ballToRemove : toRemove) {
            this.ballPaths.remove(ballToRemove);
        }

        return nodes;
    }
}