package eu.tyrano.bouncer.client;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class BallPath {

    private final double x;
    private final double y;
    private final double size;
    private int timeLeft;
    private final Color color;

    public BallPath(double x, double y, double size, Color color) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.color = color;
        this.timeLeft = 500;
    }

    public boolean isDead() {
        return this.timeLeft <= 0;
    }

    public Circle display(int width, int height) {

        double xToPrint = ((width - height) / 2d) + (height / 1000d * (this.x + 500));
        double yToPrint = height - (height / 1000d * (this.y + 500));
        double sizeToPrint = (height / 1000d * this.size);

        Circle circle = new Circle(xToPrint, yToPrint, sizeToPrint);

        circle.setFill(color);

        timeLeft--;

        circle.setOpacity(timeLeft / 500d);

        return circle;
    }
}