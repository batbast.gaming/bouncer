package eu.tyrano.bouncer.client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Bouncer extends Application {

    public static NetworkerClient networkerClient;
    public static MainLoop mainLoop;

    @Override
    public void start(Stage stage) {

        networkerClient = new NetworkerClient();

        Runtime.getRuntime().addShutdownHook(new Thread(Bouncer::shutdown));

        stage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });

        Group drawGroup = new Group();
        Group buttonGroup = new Group();
        Group parentGroup = new Group(buttonGroup, drawGroup);
        Scene scene = new Scene(parentGroup);
        stage.setTitle("Here you bounce!");
        stage.setScene(scene);
        stage.show();
        mainLoop = new MainLoop(stage, scene, drawGroup, buttonGroup);
    }

    public static void shutdown() {
        System.out.println("Closing network...");
        networkerClient.closeNetworker();
        System.out.println("Network closed");
    }

    public static void main(String[] args) {
        launch();
    }
}