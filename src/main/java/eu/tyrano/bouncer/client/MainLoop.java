package eu.tyrano.bouncer.client;

import eu.tyrano.bouncer.client.waiting.WaitingMenu;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Objects;

public class MainLoop {

    private final int screenWidth;
    private final int screenHeight;
    private final Group jfxDrawGroup;
    private double mousePressedTime;
    private final ArrayList<Ball> balls;
    private double userCode;
    private String userSide;
    private final Scene globalScene;
    private long startTime;
    private int gameLength;
    private Text textPointLeft;
    private Text textPointRight;
    private Text textTimer;
    private final Timeline timeline;
    private final Group menuGroup;
    private boolean gameEnd;
    private String gameEndMessage;
    public AudioClip boingSound;
    private final ImageView starImageView;
    private boolean starDisplayed;
    private final ArrayList<PointIcon> pointIcons;
    private final Image imageRedPoint;
    private final Image imageBluePoint;
    private final WaitingMenu waitingMenu;
    private boolean isGameStarted;
    private String canGameStart;

    public MainLoop(Stage stage, Scene scene, Group drawGroup, Group otherGroup) {

        isGameStarted = false;
        canGameStart = "";

        pointIcons = new ArrayList<>();

        starDisplayed = false;

        globalScene = scene;

        jfxDrawGroup = drawGroup;
        menuGroup = otherGroup;

        stage.setFullScreen(true);

        stage.fullScreenProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && !newValue) stage.setFullScreen(true);
        });

        globalScene.setFill(Color.WHITE);

        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();

        screenWidth = (int) screenBounds.getWidth();
        screenHeight = (int) screenBounds.getHeight();

        balls = new ArrayList<>();

        boingSound = new AudioClip(Objects.requireNonNull(this.getClass().getResource("/sounds/boing.mp3")).toExternalForm());
        Image image = new Image(Objects.requireNonNull(this.getClass().getResource("/models/star.png")).toExternalForm());
        double imageWidth = image.getWidth();
        double imageHeight = image.getHeight();
        starImageView = new ImageView(image);
        starImageView.setFitWidth(screenWidth / 20d);
        starImageView.setFitHeight(screenWidth / 20d * (imageHeight / imageWidth));

        imageRedPoint = new Image(Objects.requireNonNull(
                this.getClass().getResource("/models/plus_one_point_red.png")).toExternalForm());
        imageBluePoint = new Image(Objects.requireNonNull(
                this.getClass().getResource("/models/plus_one_point_blue.png")).toExternalForm());

        waitingMenu = new WaitingMenu(drawGroup, screenWidth, screenHeight, scene);

        timeline = new Timeline(new KeyFrame(Duration.seconds(0.01), event -> frameLoop()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    private void initScene() {
        globalScene.setFill(Color.BLACK);

        int gameHeight = screenWidth / 5;

        Rectangle rectangleGame = new Rectangle(0, screenHeight / 2d - gameHeight / 2d, screenWidth * 2, gameHeight);
        rectangleGame.setFill(Color.GREY);

        Rectangle rectangleMiddle = new Rectangle(screenWidth * 39 / 80d, screenHeight / 2d - gameHeight / 2d, screenHeight / 40d, gameHeight);
        rectangleMiddle.setFill(Color.DARKGRAY);

        menuGroup.getChildren().addAll(rectangleGame, rectangleMiddle);

        Font font = Font.font("Verdana", FontWeight.BOLD, screenHeight / 20d);

        textPointLeft = new Text();
        textPointLeft.setText("point left");
        textPointLeft.setFill(Color.WHITESMOKE);
        textPointLeft.setFont(font);
        textPointLeft.setX(screenWidth * 3 / 8d);
        textPointLeft.setY(screenHeight / 20d);
        textPointLeft.setX(textPointLeft.getX() - textPointLeft.getLayoutBounds().getWidth() / 2);
        textPointLeft.setY(textPointLeft.getY() + textPointLeft.getLayoutBounds().getHeight() / 4);

        textPointRight = new Text();
        textPointRight.setText("point right");
        textPointRight.setFill(Color.WHITESMOKE);
        textPointRight.setFont(font);
        textPointRight.setX(screenWidth * 5 / 8d);
        textPointRight.setY(screenHeight / 20d);
        textPointRight.setX(textPointRight.getX() - textPointRight.getLayoutBounds().getWidth() / 2);
        textPointRight.setY(textPointRight.getY() + textPointRight.getLayoutBounds().getHeight() / 4);

        textTimer = new Text();
        textTimer.setText("Waiting for players...");
        textTimer.setFill(Color.WHITESMOKE);
        textTimer.setFont(font);
        textTimer.setX(screenWidth / 2d);
        textTimer.setY(screenHeight * 19 / 20d);
        textTimer.setX(textTimer.getX() - textTimer.getLayoutBounds().getWidth() / 2);
        textTimer.setY(textTimer.getY() + textTimer.getLayoutBounds().getHeight() / 4);

        menuGroup.getChildren().addAll(textPointLeft, textPointRight, textTimer);
    }

    public void receiveMessage(String message) {
        switch (message.split(";")[0]) {
            case "init" -> init();
            case "balls" -> moveBalls(message);
            case "start" -> canGameStart = message;
            case "points" -> printPoints(message);
            case "end" -> {
                gameEnd = true;
                gameEndMessage = message;
            }
            default -> System.err.println("Anomaly in received message : " + message);
        }
    }

    private void gameEnd(String message) {
        String[] messages = message.split(";");

        timeline.stop();

        jfxDrawGroup.getChildren().clear();
        menuGroup.getChildren().clear();

        if (userSide.equals("left")) {
            globalScene.setFill(Integer.parseInt(messages[1]) > Integer.parseInt(messages[2]) ? Color.LIMEGREEN : Color.DARKRED);
        } else {
            globalScene.setFill(Integer.parseInt(messages[2]) > Integer.parseInt(messages[1]) ? Color.LIMEGREEN : Color.DARKRED);
        }

        jfxDrawGroup.getChildren().clear();
        menuGroup.getChildren().clear();

        Font fontPoint = Font.font("Verdana", FontWeight.BOLD, screenHeight / 8d);

        String gameMessage;

        if (globalScene.getFill().equals(Color.LIMEGREEN)) {
            gameMessage = "You won !";
        } else {
            gameMessage = "You lost !";
        }

        Text textEndGame = new Text();
        textEndGame.setText(gameMessage + "\nLEFT -> " + messages[1] + "\nRIGHT -> " + messages[2]);
        textEndGame.setFill(Color.ALICEBLUE);
        textEndGame.setFont(fontPoint);
        textEndGame.setX(screenWidth / 2d);
        textEndGame.setY(screenHeight / 2d);
        textEndGame.setX(textEndGame.getX() - textEndGame.getLayoutBounds().getWidth() / 2);
        textEndGame.setY(textEndGame.getY() + textEndGame.getLayoutBounds().getHeight() / 4);

        menuGroup.getChildren().add(textEndGame);
    }

    private void printPoints(String message) {
        String[] messages = message.split(";");

        textPointLeft.setText(messages[1]);
        textPointLeft.setX(screenWidth * 3 / 8d);
        textPointLeft.setY(screenHeight / 20d);

        textPointRight.setText(messages[2]);
        textPointRight.setX(screenWidth * 5 / 8d);
        textPointRight.setY(screenHeight / 20d);

        if (messages[4].equals("right")) {
            Image image;
            if (userSide.equals("right")) {
                image = imageBluePoint;
            } else {
                image = imageRedPoint;
            }
            pointIcons.add(new PointIcon(
                    0d, screenHeight / 2d, textPointRight.getX(), textPointRight.getY(), screenWidth, image));
        } else if (messages[4].equals("left")) {
            Image image;
            if (userSide.equals("left")) {
                image = imageBluePoint;
            } else {
                image = imageRedPoint;
            }
            pointIcons.add(new PointIcon(
                    screenWidth, screenHeight / 2d, textPointLeft.getX(), textPointLeft.getY(), screenWidth, image));
        }
    }

    private void init() {
        userCode = Math.random();
        Bouncer.networkerClient.sendMessage("init;" + userCode);
    }

    private void start(String message) {
        waitingMenu.stopTimeline();

        initScene();

        int otherSide;

        if (message.split(";")[1].equals(Double.toString(userCode))) {
            otherSide = 2;
        } else {
            otherSide = 1;
        }

        userSide = (userCode < Double.parseDouble(message.split(";")[otherSide])) ? "right" : "left";

        textPointRight.setFill(userSide.equals("right") ? Color.BLUE : Color.DARKRED);
        textPointLeft.setFill(userSide.equals("left") ? Color.BLUE : Color.DARKRED);

        int ballCount = Integer.parseInt(message.split(";")[3]);
        System.out.println("message = " + message);

        for (int i = 0; i < ballCount; i++) {
            int randomR = (int) (Math.random() * 255);
            int randomG = (int) (Math.random() * 255);
            int randomB = (int) (Math.random() * 255);
            balls.add(new Ball(20, Color.rgb(randomR, randomG, randomB)));
        }

        startTime = Long.parseLong(message.split(";")[4]);

        gameLength = Integer.parseInt(message.split(";")[5]);

        globalScene.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> mousePressedTime = System.currentTimeMillis());

        globalScene.addEventHandler(MouseEvent.MOUSE_RELEASED, this::explode);

        isGameStarted = true;
    }

    private void newStar() {
        int gameHeight = screenWidth / 5;

        double x = (Math.random() * (screenWidth - starImageView.getFitWidth()));
        double y = (Math.random() * ((gameHeight + screenHeight / 2d - gameHeight / 2d) - starImageView.getFitHeight()));
        starImageView.setX(x);
        starImageView.setY(y);
        starDisplayed = true;
    }

    private void moveBalls(String message) {
        if (!isGameStarted) {
            return;
        }

        String[] ballCoordinates = message.split(";");

        int ballIncrement = 0;
        for (int i = 0; i < ballCoordinates.length - 3; i += 2) {
            balls.get(ballIncrement).move(Double.parseDouble(ballCoordinates[i + 1]), Double.parseDouble(ballCoordinates[i + 2]));
            ballIncrement++;
        }

        if (ballCoordinates[ballCoordinates.length - 2].equals("boing")) {
            Bouncer.mainLoop.boingSound.play();
        }

        if (ballCoordinates[ballCoordinates.length - 1].equals("newStar")) {
            newStar();
        } else if (ballCoordinates[ballCoordinates.length - 1].equals("noStar")) {
            starDisplayed = false;
        } else if (!starDisplayed) {
            newStar();
        }
    }

    private void explode(MouseEvent event) {

        if (event.getX() > starImageView.getX()
                && event.getX() < starImageView.getX() + starImageView.getFitWidth()
                && event.getY() > starImageView.getY()
                && event.getY() < starImageView.getY() + starImageView.getFitHeight()) {

            Bouncer.networkerClient.sendMessage("starFound;" + userSide);
            return;
        }

        double power = (System.currentTimeMillis() - mousePressedTime) / 1000;

        if (power > 3) {
            power = 3;
        }

        double clickX = (event.getX() - ((screenWidth - screenHeight) / 2d)) * (1000f / screenHeight) - 500;
        double clickY = ((screenHeight - event.getY()) * (1000f / screenHeight)) - 500;

        if ((userSide.equals("right") && clickX > 0) || (userSide.equals("left") && clickX < 0)) {
            balls.get(1).newBallPath(clickX, clickY, 50 * power);

            Bouncer.networkerClient.sendMessage("explode;" + userSide + ";" + clickX + ";" + clickY + ";" + power);
        }
    }

    private void frameLoop() {
        if (!isGameStarted) {
            if (!canGameStart.equals("")) {
                start(canGameStart);
            } else {
                return;
            }
        }

        if (gameEnd) {
            gameEnd(gameEndMessage);
            return;
        }

        if (startTime != 0) {
            textTimer.setText("time left :\n" + (int) ((System.currentTimeMillis() - startTime) / 1000d) + " / " + gameLength);
        }

        jfxDrawGroup.getChildren().clear();

        for (Ball ball : balls) {
            jfxDrawGroup.getChildren().addAll(ball.display(screenWidth, screenHeight));
        }

        ArrayList<PointIcon> toRemove = new ArrayList<>();

        for (int i = 1; i < this.pointIcons.size() + 1; i++) {
            this.pointIcons.get(i - 1).move();
            if (this.pointIcons.get(i - 1).isDead()) {
                toRemove.add(this.pointIcons.get(i - 1));
            }
            jfxDrawGroup.getChildren().add(this.pointIcons.get(i - 1).display());
        }

        for (PointIcon pointToRemove : toRemove) {
            this.pointIcons.remove(pointToRemove);
        }

        if (starDisplayed) {
            jfxDrawGroup.getChildren().add(starImageView);
        }
    }
}