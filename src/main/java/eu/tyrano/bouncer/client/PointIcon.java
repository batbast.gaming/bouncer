package eu.tyrano.bouncer.client;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Objects;

public class PointIcon {
    private double x;
    private double y;
    private final double xStart;
    private final double yStart;
    private final double xToGo;
    private final double yToGo;
    private final ImageView icon;
    private double percent;

    public PointIcon(double x, double y, double xToGo, double yToGo, double screenWidth, Image image) {
        this.x = x;
        this.y = y;
        this.xStart = x;
        this.yStart = y;
        this.xToGo = xToGo;
        this.yToGo = yToGo;
        this.percent = 0.01;
        double imageWidth = image.getWidth();
        double imageHeight = image.getHeight();
        icon = new ImageView(image);
        icon.setFitWidth(screenWidth / 40d);
        icon.setFitHeight(screenWidth / 40d * (imageHeight / imageWidth));
    }

    public void move() {
        this.x = this.xStart + (this.xToGo - this.xStart) * percent;
        this.y = this.yStart + (this.yToGo - this.yStart) * percent;
        icon.setX(this.x);
        icon.setY(this.y);
        percent += 0.01;
        if (percent >= 0.5) {
            icon.setOpacity(1 - percent);
        }
    }

    public boolean isDead() {
        return percent >= 1;
    }

    public ImageView display() {
        return icon;
    }
}
