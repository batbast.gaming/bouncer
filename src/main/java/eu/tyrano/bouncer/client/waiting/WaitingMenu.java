package eu.tyrano.bouncer.client.waiting;

import eu.tyrano.bouncer.client.AdvancedRandom;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class WaitingMenu {

    private final Group jfxGroup;
    private final Timeline timeline;
    private final MenuFont menuFont;
    private int newColor;
    private final double[] actualColor;
    private double oldColor;
    private double colorIncrement;
    private final Scene scene;
    private int colorToChange;

    public WaitingMenu(Group group, double screenWidth, double screenHeight, Scene scene) {
        this.jfxGroup = group;
        this.scene = scene;
        this.colorIncrement = 0;

        int gameScreenX = (int) ((screenWidth - screenHeight) / 2);

        this.menuFont = new MenuFont((int) screenHeight, gameScreenX, 0, 1, 8);
        this.actualColor = menuFont.calculateDraw();
        scene.setFill(Color.rgb((int) actualColor[0], (int) actualColor[1], (int) actualColor[2]));
        this.newColor = (int) AdvancedRandom.random(0, 255);
        this.colorToChange = 0;
        this.oldColor = actualColor[colorToChange];

        this.timeline = new Timeline(new KeyFrame(Duration.seconds(0.01), event -> loop()));
        this.timeline.setCycleCount(Animation.INDEFINITE);
        this.timeline.play();
    }

    public void stopTimeline() {
        timeline.stop();
    }

    private void loop() {
        jfxGroup.getChildren().clear();
        menuFont.setColor((int) actualColor[0],(int) actualColor[1],(int) actualColor[2]);
        double[] color = menuFont.changeDraw(0.01, 20);
        scene.setFill(Color.rgb((int) color[0], (int) color[1], (int) color[2]));
        menuFont.rotate(10);
        jfxGroup.getChildren().addAll(menuFont.display());


        this.colorIncrement += 0.003;
        this.actualColor[colorToChange] = Math.abs((colorIncrement * newColor) + ((1 - colorIncrement) * oldColor));

        if (this.actualColor[colorToChange] > 255) {
            this.actualColor[colorToChange] = 255;
        } else if (this.actualColor[colorToChange] < 0) {
            this.actualColor[colorToChange] = 0;
        }

        if (colorIncrement >= 1) {
            colorToChange = (int) AdvancedRandom.random(0, 3);
            if (colorToChange == 3) {
                colorToChange = 2;
            }
            oldColor = actualColor[colorToChange];

            newColor = (int) AdvancedRandom.random(0, 255);

            colorIncrement = 0;
        }
    }
}
