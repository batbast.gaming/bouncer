package eu.tyrano.bouncer.server;

import java.util.ArrayList;

import static java.lang.Math.*;


public class Ball {

    private double x;
    private double y;
    private final double size;
    private final double weight;
    private double vectorX = 0;
    private double vectorY = 0;
    private double futureVectorX = 0;
    private double futureVectorY = 0;
    private boolean canContact;

    public Ball(double x, double y, double size, double weight) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.weight = weight;
        this.canContact = false;
    }

    public void contact(double[] vectorXYToAdd) {
        this.vectorX = futureVectorX + vectorXYToAdd[0];
        this.vectorY = futureVectorY + vectorXYToAdd[1];
        MainLoop.doBoing = true;
    }

    public double[] rotate(double pointX, double pointY, double angle) {
        double distance = dist(0, 0, pointX, pointY);
        double firstAngle;

        if (pointX != 0) {
            firstAngle = atan(pointY / pointX);
        } else {
            firstAngle = PI / 2;
        }

        double finalAngle = firstAngle + angle;
        double finalX = cos(finalAngle) * distance;
        double finalY = sin(finalAngle) * distance;

        return new double[]{finalX, finalY};
    }

    public Ball testForBounce(ArrayList<Ball> balls) {

        ArrayList<Ball> tmpBalls = new ArrayList<>(balls);

        tmpBalls.remove(this);

        for (Ball ball : tmpBalls) {
            if (dist(ball.getX(), ball.getY(), this.getX(), this.getY()) < ball.getSize() + this.size) {
                if (this.canContact) {
                    this.canContact = false;
                    return ball;
                } else {
                    return null;
                }
            }
        }

        canContact = true;
        return null;
    }

    public double[] takeContact(double enemyVectorX, double enemyVectorY, double enemyX, double enemyY, double enemyWeight, double enemySize) {
        //the moving object, here, is "enemy".
        //for these calculations, "this" is not moving.

        double neededRotation;

        if (enemyVectorX != 0) {
            neededRotation = -atan((enemyVectorY) / (enemyVectorX));
        } else {
            if (enemyY > this.y) {
                neededRotation = PI / 2;
            } else {
                neededRotation = -PI / 2;
            }
        }

        boolean rotationAdded = false;
        if (this.x < enemyX) {
            neededRotation += PI;
            rotationAdded = true;
        }

        double[] rotatedPoint = rotate(enemyX - this.x, enemyY - this.y, neededRotation);
        double rotatedEnemyY = rotatedPoint[1];

        boolean symmetry = ((rotatedEnemyY > 0 || enemyY > this.y) && !rotationAdded);

        double[] rotatedVector = rotate(enemyVectorX, enemyVectorY, neededRotation);
        double rotatedVectorX = rotatedVector[0];

        double weightCoefficient = enemyWeight / this.weight;

        double theta = acos(rotatedEnemyY / (enemySize + this.size));

        double enemyRotatedVectorX = ((weightCoefficient - 1 + (2 * pow(cos(theta), 2))) * rotatedVectorX) / (weightCoefficient + 1);
        double enemyRotatedVectorY = (2 * sin(theta) * cos(theta) * rotatedVectorX) / (weightCoefficient + 1);
        if (symmetry) {
            enemyRotatedVectorY = -enemyRotatedVectorY;
        }

        double thisRotatedVectorX = (2 * weightCoefficient * rotatedVectorX * pow(sin(theta), 2)) / (weightCoefficient + 1);
        double thisRotatedVectorY = -(2 * weightCoefficient * rotatedVectorX * sin(theta) * cos(theta)) / (weightCoefficient + 1);
        if (symmetry) {
            thisRotatedVectorY = -thisRotatedVectorY;
        }

        double[] thisVectors = rotate(thisRotatedVectorX, thisRotatedVectorY, -neededRotation);

        futureVectorX = thisVectors[0];
        futureVectorY = thisVectors[1];

        double[] finalVectors = rotate(enemyRotatedVectorX, enemyRotatedVectorY, -neededRotation);

        return new double[]{finalVectors[0], finalVectors[1]};
    }

    public double getVectorX() {
        return vectorX;
    }

    public double getVectorY() {
        return vectorY;
    }

    private double dist(double xFrom, double yFrom, double xTo, double yTo) {
        return sqrt(pow((xTo - xFrom), 2) + pow((yTo - yFrom), 2));
    }

    public void push(double sourceX, double sourceY, double power) {

        power = (power / dist(this.x, this.y, sourceX, sourceY)) * 300;

        double alpha = atan2(this.y - sourceY, this.x - sourceX);

        double poweredVectorX = power * 10 * cos(alpha);
        double poweredVectorY = power * 10 * sin(alpha);

        this.vectorX = this.vectorX + poweredVectorX;
        this.vectorY = this.vectorY + poweredVectorY;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getSize() {
        return size;
    }

    public void move(double upLimit, double rightLimit, double downLimit, double leftLimit) {
        if (vectorX > 1000) {
            vectorX = 1000;
        }

        if (vectorY > 1000) {
            vectorY = 1000;
        }

        this.x = this.x + this.vectorX / 100;
        this.y = this.y + this.vectorY / 100;
        double gravity = 9.81;
        this.vectorY = this.vectorY - gravity;
        if (this.y < downLimit) {
            this.y = downLimit;
            this.vectorY = -this.vectorY;
            MainLoop.doBoing = true;
        }
        if (this.y > upLimit) {
            this.y = upLimit;
            this.vectorY = -this.vectorY;
            MainLoop.doBoing = true;
        }
        if (this.x < leftLimit) {
            this.x = leftLimit;
            this.vectorX = -this.vectorX;
            MainLoop.doBoing = true;
            MainLoop.addPlayerPoint("right", 1);
        }
        if (this.x > rightLimit) {
            this.x = rightLimit;
            this.vectorX = -this.vectorX;
            MainLoop.doBoing = true;
            MainLoop.addPlayerPoint("left", 1);
        }
    }

    public double getWeight() {
        return this.weight;
    }
}