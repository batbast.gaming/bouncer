package eu.tyrano.bouncer.server;

public class Bouncer {

    public static NetworkerServer networkerServer;

    public static void main(String[] args) {
        networkerServer = new NetworkerServer();
        MainLoop.looper();
        Runtime.getRuntime().addShutdownHook(new Thread(Bouncer::shutdown));
    }

    public static void shutdown() {
        System.out.println("Closing network...");
        networkerServer.closeNetworker();
        System.out.println("Network closed");
    }
}