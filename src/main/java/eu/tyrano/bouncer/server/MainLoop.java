package eu.tyrano.bouncer.server;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainLoop {

    private static ArrayList<Ball> balls;
    private static double initReceived;
    private static int ballCount;
    private static int leftPoints;
    private static int rightPoints;
    private static long startTime;
    private static int gameLength;
    public static boolean doBoing;
    private static double networkRate;
    private static boolean waitingForStar;

    public static void looper() {

        leftPoints = 0;
        rightPoints = 0;

        //in seconds
        gameLength = 60;

        balls = new ArrayList<>();

        initReceived = 0;

        ballCount = 3;

        doBoing = false;

        waitingForStar = false;

        Bouncer.networkerServer.sendMessage("init");
    }

    private static void initGame() {

        for (int i = 0; i < ballCount; i++) {
            balls.add(new Ball(((1000d / ballCount) * i) - (1000d / ballCount), 0, 20, 1));
        }

        Timer simulationTimer = new Timer();

        simulationTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                frameLoop();
            }
        }, 0, 10);

        //packets sent per second to users.
        networkRate = 40;

        Timer networkTimer = new Timer();

        networkTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                networkLoop();
            }
        }, 0, (int) (1000 / networkRate));
    }

    public static void explode(String data) {

        //"explode;player;clickX;clickY;power"

        String[] dataSplit = data.split(";");

        String player;
        double clickX;
        double clickY;
        double power;

        if (dataSplit.length == 5) {
            player = dataSplit[1];
            clickX = Double.parseDouble(dataSplit[2]);
            clickY = Double.parseDouble(dataSplit[3]);
            power = Double.parseDouble(dataSplit[4]);
        } else {
            System.err.println("Data received by player " + dataSplit[1] + " are not regular.");
            return;
        }

        if (player.equals("right")) {
            if (clickX < 0) {
                System.err.println("Player " + player + " clicked at the wrong place.");
                return;
            }
        } else if (player.equals("left")) {
            if (clickX > 0) {
                System.err.println("Player " + player + " clicked at the wrong place.");
                return;
            }
        } else {
            System.err.println("Player " + player + " do not exist.");
            return;
        }

        if (power > 3) {
            power = 3;
        }

        for (Ball ball : balls) {
            ball.push(clickX, clickY, power * 100);
        }
    }

    private static void networkLoop() {
        //balls;ballAX;ballAY;ballBX;ballBY;ballCX;ballCY;...;boing/noBoing
        StringBuilder message = new StringBuilder("balls;");

        for (Ball ball : balls) {
            message.append(ball.getX()).append(";").append(ball.getY()).append(";");
        }

        if (doBoing) {
            message.append("boing;");
            doBoing = false;
        } else {
            message.append("noBoing;");
        }

        if (Math.random() <= 0.1d / networkRate && !waitingForStar) {
            System.out.println("star added!");
            message.append("newStar");
            waitingForStar = true;
        } else if (waitingForStar) {
            message.append("star");
        } else {
            message.append("noStar");
        }

        Bouncer.networkerServer.sendMessage(String.valueOf(message));

        if (System.currentTimeMillis() > startTime + (gameLength * 1000d)) {
            Bouncer.networkerServer.sendMessage("end;" + leftPoints + ";" + rightPoints);
            System.exit(0);
        }
    }

    public static void addPlayerPoint(String player, int quantity) {
        if (player.equals("left")) {
            leftPoints += quantity;
        } else {
            rightPoints += quantity;
        }

        Bouncer.networkerServer.sendMessage("points;" + leftPoints + ";" + rightPoints + ";" + quantity + ";" + player);
    }

    private static void frameLoop() {

        double down = -200;
        double right = 1000;
        double up = 200;
        double left = -1000;

        for (Ball ball : balls) {
            ball.move(up, right, down, left);
            Ball ballToBounce = ball.testForBounce(balls);
            if (ballToBounce != null) {
                double[] ball1Contact = ball.takeContact(ballToBounce.getVectorX(), ballToBounce.getVectorY(), ballToBounce.getX(), ballToBounce.getY(), ballToBounce.getWeight(), ballToBounce.getSize());
                double[] ball2Contact = ballToBounce.takeContact(ball.getVectorX(), ball.getVectorY(), ball.getX(), ball.getY(), ball.getWeight(), ball.getSize());
                ballToBounce.contact(ball1Contact);
                ball.contact(ball2Contact);
            }
        }
    }

    public static void receiveMessage(String message) {
        switch (message.split(";")[0]) {
            case "explode" -> explode(message);
            case "init" -> {
                double parsedMessage = Double.parseDouble(message.split(";")[1]);
                if (initReceived == 0) {
                    initReceived = parsedMessage;
                } else {
                    if (initReceived == parsedMessage) {
                        initReceived = 0;
                        Bouncer.networkerServer.sendMessage("init");
                    } else {
                        startTime = System.currentTimeMillis();
                        Bouncer.networkerServer.sendMessage("start;" + initReceived + ";" + parsedMessage + ";" + ballCount
                                + ";" + startTime + ";" + gameLength);
                        initGame();
                    }
                }
            }
            case "starFound" -> {
                addPlayerPoint(message.split(";")[1], 5);
                waitingForStar = false;
            }
        }
    }
}