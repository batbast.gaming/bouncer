package eu.tyrano.bouncer.server;

import com.rabbitmq.client.*;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeoutException;

public class NetworkerServer {

    private final String[] channelSendIDs;
    private final String[] channelReceiveIDs;
    private final Channel[] channelsSend;
    private final Channel[] channelsReceive;
    private Connection connection;

    public NetworkerServer() {

        channelSendIDs = new String[]{"toClient"};

        channelReceiveIDs = new String[]{"toServer"};

        channelsSend = new Channel[channelSendIDs.length];

        channelsReceive = new Channel[channelReceiveIDs.length];

        try {
            initRabbit();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    private SSLContext initKeyStore() throws Exception {
        char[] trustPassphrase = "isur2reimg8k53".toCharArray();
        KeyStore rabbitmqKeyStore = KeyStore.getInstance("JKS");
        String keyStoreLocation = "/rabbitmq/tyrano.eu.jks";
        rabbitmqKeyStore.load(this.getClass().getResourceAsStream(keyStoreLocation), trustPassphrase);

        TrustManagerFactory trustFactory = TrustManagerFactory.getInstance("SunX509");
        trustFactory.init(rabbitmqKeyStore);

        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(null, trustFactory.getTrustManagers(), null);

        return sslContext;
    }

    private void initRabbit() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("bouncer.tyrano.eu");
        factory.setPort(5671);
        factory.setVirtualHost("bouncer");
        factory.setUsername("bouncer_server");
        factory.setPassword("0LR7yEH7jc0rJPj80n1bO02K7Mgo2fU");
        factory.enableHostnameVerification();

        try {
            factory.useSslProtocol(initKeyStore());
        } catch (Exception e) {
            e.printStackTrace();
        }

        connection = factory.newConnection();

        // Send message
        for (int i = 0; i < channelsSend.length; i++) {
            channelsSend[i] = connection.createChannel();
            channelsSend[i].queueDeclare(channelSendIDs[i], false, false, true, null);
        }

        // Receive message
        for (int i = 0; i < channelsReceive.length; i++) {
            channelsReceive[i] = connection.createChannel();
            channelsReceive[i].exchangeDeclare(channelReceiveIDs[i], BuiltinExchangeType.FANOUT);
            String queueName = channelsReceive[i].queueDeclare().getQueue();
            channelsReceive[i].queueBind(queueName, channelReceiveIDs[i], "");
            receiveMessage(i, queueName);
        }
    }

    public void sendMessage(String message) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        //System.out.println(dtf.format(now) + " sending message = " + message);
        //System.out.println();

        try {
            channelsSend[0].basicPublish(channelSendIDs[0], "", null, message.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void receiveMessage(int i, String queueName) {

        DeliverCallback deliverCallback = (consumerTag, delivery) ->
                MainLoop.receiveMessage(new String(delivery.getBody(), StandardCharsets.UTF_8));

        try {
            channelsReceive[i].basicConsume(queueName, true, deliverCallback, consumerTag -> {});
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void closeNetworker() {
        try {
            for (Channel channel : channelsSend) {
                channel.close();
            }
        } catch (TimeoutException | IOException ignored) {
            // channel already closed
        }

        try {
            connection.close();
        } catch (IOException ignored) {
            // connection already closed
        }
    }
}