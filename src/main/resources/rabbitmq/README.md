# Import public key to keystore
cert1.pem is the public key, not the private key.

```bash
$ keytool -import -trustcacerts -alias tyrano.eu -file cert1.pem -keystore tyrano.eu.jks
```